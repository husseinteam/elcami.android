package ist.hussein.elcami.views;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import java.util.Map;

import ist.hussein.elcami.R;
import ist.hussein.elcami.components.ECCircleView;
import ist.hussein.elcami.components.ECTextView;
import ist.hussein.elcami.engines.zikhir.ECZikhirEngine;
import ist.hussein.elcami.models.ECZikhir;
import ist.hussein.elcami.protocols.CallableVector;


public class ECZikhirView extends ConstraintLayout {

	private ECTextView zikhirTitle;
	private ECTextView zikhir;
	private ECCircleView ecCircleView;
	private Context mContext;

	public ECZikhirView(Context context) {
		super(context);
		mContext = context;
	}

	public ECZikhirView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	public ECZikhirView(Context context, @Nullable AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
	}

	private void initLocals() {
		this.zikhirTitle = (ECTextView) findViewById(R.id.textZikhirTitle);
		this.zikhir = (ECTextView) findViewById(R.id.textZikhir);
		this.ecCircleView = (ECCircleView) findViewById(R.id.zikhirCircle);
	}

	private void initInteractions(int currentProgress, int secondaryIndex) {
		final ECTextView zikhirTextView = this.zikhir;
		final ECCircleView circleView = this.ecCircleView;

		setCurrentProgress(currentProgress);
		setSecondaryIndex(secondaryIndex);

		ECZikhir currentZikhir = ECZikhirEngine.initZikhir(currentProgress);
		zikhirTextView.setText(currentZikhir.getTranscript());

		circleView.setCount(secondaryIndex);
		circleView.setPrimaryProgressRatio(
				ECZikhirEngine.getPrimaryProgressRatio());
		circleView.setLabelColor(Color.parseColor("#D8001F"));
		circleView.setOnClick(
				new CallableVector<ECCircleView>() {
					@Override
					public void Calls(ECCircleView self) {
						super.Calls(self);
						ECZikhirEngine.nextZhikir(
								new CallableVector<Map.Entry<Boolean, ECZikhir>>() {
									@Override
									public void Calls(Map.Entry<Boolean, ECZikhir> self) {
										super.Calls(self);
										if (self.getKey()) {
											circleView.resetCount();
										} else {
											circleView.incrementCount();
										}
										setSecondaryIndex(circleView.getCount());
										setCurrentProgress(ECZikhirEngine.getPrimaryProgress());
										zikhirTextView.setText(self.getValue().getTranscript());
										circleView.setPrimaryProgressRatio(
												ECZikhirEngine.getPrimaryProgressRatio());
									}
								}
								, new CallableVector<Integer>() {
									@Override
									public void Calls(Integer self) {
										super.Calls(self);
										new Handler(Looper.getMainLooper()).post(new Runnable() {
											@Override
											public void run() {
												Toast toast1 =
														Toast.makeText(mContext,
																"El-Fatiha",
																Toast.LENGTH_LONG);
												toast1.show();
											}
										});
									}
								});
					}
				});
		circleView.setOnLongClick(
				new CallableVector<ECCircleView>() {
					@Override
					public void Calls(ECCircleView self) {
						super.Calls(self);
						self.resetListeners();
						initInteractions(1, 1);
					}
				}
		);
	}

	private void setCurrentProgress(int progress) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("currentProgress", progress);
		editor.apply();
	}

	private void setSecondaryIndex(int index) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("secondaryIndex", index);
		editor.apply();
	}

	protected void initFinal() {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
		int currentProgress = settings.getInt("currentProgress", 1);
		int secondaryIndex = settings.getInt("secondaryIndex", 1);
		this.initLocals();
		this.initInteractions(currentProgress, secondaryIndex);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		this.initFinal();
	}
}
