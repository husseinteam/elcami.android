
package ist.hussein.elcami.engines.restClient;

import org.json.JSONObject;

interface RestClientInterface {
    void AddParam(String name, String value);

    void AddHeader(String name, String value);

    JSONObject getJson();
}