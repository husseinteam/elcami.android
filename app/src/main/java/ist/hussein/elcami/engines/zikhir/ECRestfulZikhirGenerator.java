package ist.hussein.elcami.engines.zikhir;

import ist.hussein.elcami.engines.restClient.HttpMethod;
import ist.hussein.elcami.engines.restClient.RestClient;
import ist.hussein.elcami.models.ECZikhir;
import ist.hussein.elcami.protocols.ModelGeneratorInterface;

public class ECRestfulZikhirGenerator implements ModelGeneratorInterface<ECZikhir> {
	@Override
	public ECZikhir[] generate() {
		RestClient rc = new RestClient("zikhirs", HttpMethod.GET);
		return new ECZikhir[0];
	}

}
