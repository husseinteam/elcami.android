package ist.hussein.elcami.engines.zikhir;

import java.util.AbstractMap;
import java.util.Map;

import ist.hussein.elcami.models.ECZikhir;
import ist.hussein.elcami.models.ECZikirProgress;
import ist.hussein.elcami.protocols.CallableVector;
import ist.hussein.elcami.protocols.ModelGeneratorInterface;

public class ECZikhirEngine {

	private static final ModelGeneratorInterface<ECZikhir> zikhirGenerotor
			= new ECConcreteZikhirGenerator();
	private static ECZikirProgress ecZikhirProgress = new ECZikirProgress();
	private static int totalZikhirCount = 0;
	private static ECZikhir[] zikhirs = zikhirGenerotor.generate();

	private static boolean proceededToNextZikhir() {
		int till = 0;
		for (int j = 0; j <= ecZikhirProgress.getSecondaryIndex(); j++) {
			till += zikhirs[j].getCount();
		}
		return till == ecZikhirProgress.getPrimaryProgress();
	}

	public static ECZikhir initZikhir(int currentProgress) {
		ecZikhirProgress.reset();
		totalZikhirCount = 0;
		for (ECZikhir zikhir: zikhirs) {
			totalZikhirCount += zikhir.getCount();
			if (totalZikhirCount < currentProgress) {
				ecZikhirProgress.incrementSecondaryIndex();
				ecZikhirProgress.setPrimaryProgress(currentProgress);
			}
		}

		return zikhirs[ecZikhirProgress.getSecondaryIndex()];
	}

	public static float getPrimaryProgressRatio() {
		return ecZikhirProgress.getPrimaryProgress() / (1.0f*totalZikhirCount);
	}

	public static int getPrimaryProgress() {
		return ecZikhirProgress.getPrimaryProgress();
	}
	public static void nextZhikir(
			CallableVector<Map.Entry<Boolean, ECZikhir>> currentVector,
			CallableVector<Integer> completedVector) {
		Boolean proceededToNext = proceededToNextZikhir();
		if (proceededToNext) {
			ecZikhirProgress.incrementSecondaryIndex();
		}
		ecZikhirProgress.incrementPrimaryProgress();
		if (ecZikhirProgress.getSecondaryIndex() == totalZikhirCount) {
			completedVector.Calls(ecZikhirProgress.getSecondaryIndex());
		} else {
			Map.Entry<Boolean, ECZikhir> entry =
					new AbstractMap.SimpleEntry<>(
							proceededToNext, zikhirs[ecZikhirProgress.getSecondaryIndex()]);
			currentVector.Calls(entry);
			ECZikhirProgressEngine.persistDailyProgress(ecZikhirProgress);
		}
	}

}
