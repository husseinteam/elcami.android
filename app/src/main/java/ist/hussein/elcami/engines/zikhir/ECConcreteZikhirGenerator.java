package ist.hussein.elcami.engines.zikhir;

import ist.hussein.elcami.models.ECZikhir;
import ist.hussein.elcami.protocols.ModelGeneratorInterface;

class ECConcreteZikhirGenerator implements ModelGeneratorInterface<ECZikhir> {
	@Override
	public ECZikhir[] generate() {
		return new ECZikhir[]{
				new ECZikhir("BİSMİLLAHİRRAHMANİRRAHİM"),
				new ECZikhir("Kelime-i Şehâdet", "Eşhedu en lâ ilâhe illallâh ve eşhedü enne Muhammeden Abdühü ve Resülühü", 3),
				new ECZikhir("Estâğfirullah", 70),
				new ECZikhir("Fatiha-ı Şerife", "Bismillahirrahmânirrahîm.. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn"),
				new ECZikhir("Amen er resulü", "Bismillahirrahmânirrahîm.. Amenerrasulü bima ünzile ileyhi mirrabbihi vel mü’minun, küllün amene billahi vemelaiketihi ve kütübihi ve rusülih, la nüferriku beyne ehadin min rusülih, ve kalu semi’na ve ata’na gufraneke rabbena ve ileykelmesir. La yükellifullahü nefsenilla vüs’aha, leha ma kesebet ve aleyha mektesebet, rabbena latüahızna innesiyna ev ahta’na, rabbena vela tahmil aleyna ısran kema hameltehü alelleziyne min gablina, rabbena vela tühammilna, mala takatelena bih, va’fü anna, vağfirlena, verhamna, ente mevlana fensurna alel gavmil kafiriyn. Amîn"),
				new ECZikhir("Inşirah", "Bismillahirrahmânirrahîm. Elem neşrah leke sadrek Ve vada'na 'anke vizreke Elleziy enkada zahreke Ve refa'na leke zikreke Feinne me'al'usri yüsren İnne me'al'usri yüsren Feiza ferağte fensab Ve ila rabbike ferğab. Allâh-ü Ekber", 7),
				new ECZikhir("İhlas-ı Şerif", "Bismillahirrahmânirrahîm.. Kul hüvellâhü ehad. Allâhüssamed. Lem yelid ve lem yûled. Ve lem yekün lehû küfüven ehad. Allâh-ü Ekber", 11),
				new ECZikhir("Felak", "Bismillahirrahmânirrahîm.. Kul e'ûzü birabbil felak. Min şerri mâ halak. Ve min şerri ğasikın izâ vekab. Ve min şerrinneffâsâti fil'ukad. Ve min şerri hâsidin izâ hased. Allâh-ü Ekber"),
				new ECZikhir("Nas", "Bismillahirrahmânirrahîm.. Kul e'ûzü birabbinnâs. Melikinnâs. İlâhinnâs. Min şerrilvesvâsilhannâs. Ellezî yüvesvisü fî sudûrinnâsi. Minelcinneti vennâs. Allâh-ü Ekber"),
				new ECZikhir("Kelime-i Tevhid", "Lâ İlâhe İllallah", 9),
				new ECZikhir("Kelime-i Tevhid", "Lâ İlâhe İllallah Muhammedûn Resulullah"),
				new ECZikhir("Salavat-ı Şerife", "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve sellim", 10),
				new ECZikhir("BAĞIŞLAMA", "Bu okuduklarımızdan hâsıl olan sevabı bilhassa Efendimizin (Sallallahü Aleyhi ve Sellem) Mübarek Rûh-u Şerîfine, Enbiya ve Evliyaların da rûhlarına, hassaten Şâh-ı Nakşibendi Hz.lerinin Rûh-u Şerîfine, Mevlânâ Şeyh Abdullah el Fâizi Dağıstanî Hz.lerinin Rûh-u Şerîfine, Mevlânâ Sultan-u Evliya Şeyh Muhammed Nazım El Hakkani Hz.lerinin Ruh-u Şerîfine hediyye eyledim. Ya Rabbi sen vâsıl eyle. Âmin..."),
				new ECZikhir("Fatiha-ı Şerife", "Bismillahirrahmânirrahîm.. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn"),
				new ECZikhir("Lafz-ı Celil", "Allah", 1500),
				new ECZikhir("Kelime-i Tevhid", "Lâ İlâhe İllallah", 100),
				new ECZikhir("Salavat-ı Şerife", "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve sellim", 300),
				new ECZikhir("İhlas-ı Şerif", "Bismillahirrahmânirrahîm.. Kul hüvellâhü ehad. Allâhüssamed. Lem yelid ve lem yûled. Ve lem yekün lehû küfüven ehad. Allâh-ü Ekber", 100),
				new ECZikhir("Salavat-ı Şerife", "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve sellim", 100),
				new ECZikhir("BAĞIŞLAMA", "Bu okuduklarımızdan hâsıl olan sevabı bilhassa Efendimizin (Sallallahü Aleyhi ve Sellem) Mübarek Rûh-u Şerîfine, Enbiya ve Evliyaların da rûhlarına, hassaten Şâh-ı Nakşibendi Hz.lerinin Rûh-u Şerîfine, Mevlânâ Şeyh Abdullah el Fâizi Dağıstanî Hz.lerinin Rûh-u Şerîfine, Mevlânâ Sultan-u Evliya Şeyh Muhammed Nazım El Hakkani Hz.lerinin Ruh-u Şerîfine hediyye eyledim. Ya Rabbi sen vâsıl eyle. Âmin..."),
				new ECZikhir("Fatiha-ı Şerife", "Bismillahirrahmânirrahîm.. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn")
		};
	}

}
