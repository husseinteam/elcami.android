package ist.hussein.elcami.engines.restClient;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import ist.hussein.elcami.protocols.CallableInterface;

public class RestClient extends AsyncTask<Void, Object, Void> implements RestClientInterface, CallableInterface<RestClientInterface> {

    private String url;
    private HttpMethod method;

    private String charset = "UTF-8";
    private HttpURLConnection conn;
    private DataOutputStream wr;
    private StringBuilder response = new StringBuilder();
    private URL urlObj;
    private JSONObject jObj = null;
    private StringBuilder sbParams;
    private String paramsString;
    private HashMap<String, String> parameters;
    private HashMap<String, String> headers;
    private JSONObject json;

    public RestClient(String route, HttpMethod method) {
        this.url = "http://192.168.1.50:99/rpi/" + route;
        this.method = method;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {

        sbParams = new StringBuilder();

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            try {
                sbParams.append(entry.getKey()).append("=")
                        .append(URLEncoder.encode(entry.getValue(), charset));
                sbParams.append("&");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        sbParams.deleteCharAt(sbParams.length() -1);
        try {
            urlObj = new URL(url);

            conn = (HttpURLConnection) urlObj.openConnection();
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
            conn.setDoOutput(true);

            switch (this.method) {
                case GET:
                    conn.setRequestMethod("GET");
                    break;
                case POST:
                    conn.setRequestMethod("POST");
                    break;
                case PUT:
                    conn.setRequestMethod("PUT");
                    break;
                case DELETE:
                    conn.setRequestMethod("DELETE");
                    break;
                default:
                    conn.setRequestMethod("GET");
            }

            conn.setRequestProperty("Accept-Charset", charset);
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);

            conn.connect();

            paramsString = sbParams.toString();

            wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(paramsString);
            wr.flush();
            wr.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            //response from the server
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.disconnect();
        return null;
    }

    @Override
    protected void onPostExecute(Void result){
        try {
            json = new JSONObject(response.toString());
            this.Calls(this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Calls(RestClientInterface self) {

    }

    @Override
    public void AddParam(String name, String value) {
        parameters.put(name, value);
    }

    @Override
    public void AddHeader(String name, String value) {
        headers.put(name, value);
    }

    @Override
    public JSONObject getJson() {
        return json;
    }
}

