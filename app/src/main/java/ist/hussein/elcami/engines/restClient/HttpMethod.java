package ist.hussein.elcami.engines.restClient;

public enum HttpMethod {

    GET,
    POST,
    PUT,
    DELETE

}
