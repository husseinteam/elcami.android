package ist.hussein.elcami.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import ist.hussein.elcami.R;
import ist.hussein.elcami.protocols.CallableVector;

public class ECCircleView extends View {

	private @Nullable AttributeSet attributeSet;
	private Context context;

	private int strokeWidth;
	private int margins;
	private float radius;
	private float horizontalRadius;
	private ECShapesEnum ecShapesEnum;

	private void initShapes() {
		arcPaint = new Paint() {
			{
				setDither(true);
				setStyle(Paint.Style.STROKE);
				setStrokeCap(Paint.Cap.ROUND);
				setStrokeJoin(Paint.Join.ROUND);
				setColor(Color.BLUE);
				setStrokeWidth(strokeWidth / 2);
				setAntiAlias(true);
				setAlpha(114);
			}
		};
		outerCirclePaint = new Paint() {
			{
				setDither(true);
				setStyle(Paint.Style.STROKE);
				setStrokeWidth(strokeWidth);
				setStrokeCap(Paint.Cap.ROUND);
				setStrokeJoin(Paint.Join.ROUND);
				setColor(Color.parseColor("#3BAA3A"));
				setAntiAlias(true);
				setAlpha(114);
			}
		};
	}

	private void initAttributes() {
		if (attributeSet == null) {
			return;
		}
		TypedArray a = context.getTheme().obtainStyledAttributes(
				attributeSet,
				R.styleable.ECCircleView,
				0, 0);

		try {
			int val = a.getInt(R.styleable.ECCircleView_shape, 0);
			strokeWidth = a.getInt(R.styleable.ECCircleView_strokeWidth, 4);
			margins = a.getInt(R.styleable.ECCircleView_margins, 20);
			radius = a.getFloat(R.styleable.ECCircleView_radius, 1.0f);
			horizontalRadius = a.getFloat(R.styleable.ECCircleView_horizontalRadius, 2.0f);
			ecShapesEnum = val == 0 ? ECShapesEnum.CIRCLE : ECShapesEnum.ELLIPSE;
		} finally {
			a.recycle();
		}
	}

	private void initFinal() {
		this.initAttributes();
		this.initShapes();
		this.resetCount();
	}

	public void setCount(int count) {
		this.count = count;
		this.labelText = Integer.toString(this.count);
		this.invalidate();
	}

	public int getCount() {
		return this.count;
	}

	public ECCircleView(Context context) {
		super(context);
		this.context = context;
		this.initFinal();
	}

	public ECCircleView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		this.attributeSet = attrs;
		this.initFinal();
	}

	public ECCircleView(Context context, @Nullable AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		this.attributeSet = attrs;
		this.initFinal();
	}

	private Rect rectArc = new Rect();
	private Paint arcPaint;
	private Paint outerCirclePaint;
	private String labelText;
	private Paint labelPaint;
	private Rect labelBounds = new Rect();
	private int count = 0;
	private float primaryProgressRatio = 0.0f;

	public void setOnClick(final CallableVector<ECCircleView> cli) {
		this.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ECCircleView ev = (ECCircleView)v;
				cli.Calls(ev);
			}
		});
	}

	public void setOnLongClick(final CallableVector<ECCircleView> cli) {
		this.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				ECCircleView ev = (ECCircleView)v;
				cli.Calls(ev);
				return true;
			}
		});
	}

	public void setLabelColor(final @ColorInt int color) {
		labelPaint = new Paint() {
			{
				setDither(true);
				setStyle(Paint.Style.STROKE);
				setStrokeCap(Paint.Cap.ROUND);
				setStrokeJoin(Paint.Join.ROUND);
				setColor(color);
				setAntiAlias(true);
				setTypeface(Typeface.createFromAsset(getContext().getAssets(), "OpenSans/OpenSans-Light.ttf"));
				setTextSize(99.0f);
				setAlpha(114);
			}
		};
	}

	public void incrementCount() {
		this.setCount(this.count + 1);
	}

	public void resetCount() {
		this.setCount(1);
	}

	public void resetListeners() {
		this.setCount(1);
		this.setOnClickListener(null);
	}

	public void setPrimaryProgressRatio(float progress) {
		this.primaryProgressRatio = progress;
	}

	private RectF getDrawingRectF(int diffw, int diffh) {
		getDrawingRect(rectArc);
		rectArc.inset((int)(diffw * 1.1f), (int)(diffh * 1.1f));
		return new RectF(rectArc);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int w = getWidth();
		int h = getHeight();

		int pl = getPaddingLeft();
		int pr = getPaddingRight();
		int pt = getPaddingTop();
		int pb = getPaddingBottom();

		int diffw = pl + pr + strokeWidth + margins;
		int diffh = pt + pb + strokeWidth + margins;

		int usableWidth = w - diffw;
		int usableHeight = h - diffh;

		int radius = Math.min(usableWidth, usableHeight) / 2;
		int cx = (diffw + usableWidth) / 2;
		int cy = (diffh + usableHeight) / 2;

		int sweepAngle = (int) (360 * primaryProgressRatio);
		canvas.drawCircle(cx, cy, radius, outerCirclePaint);
		canvas.drawArc(getDrawingRectF(diffw, diffh), 0.0f, sweepAngle, false, arcPaint);
		if (labelPaint != null) {
			labelPaint.getTextBounds(labelText, 0, labelText.length(), labelBounds);
			canvas.drawText(labelText, cx - labelBounds.centerX(),
					cy + labelBounds.height()/2, labelPaint);
		}
	}

}
