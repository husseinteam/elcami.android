package ist.hussein.elcami.adapters;

import ist.hussein.elcami.R;

enum ECPagerEnum {
	ZIKIRLER(R.string.zikhir, R.layout.view_zikhirs),
	VAKITLER(R.string.prayer_times, R.layout.view_prayer_times),
	DEVLETLER(R.string.regions, R.layout.view_regions);

	private int mTitleResId;
	private int mLayoutResId;

	ECPagerEnum(int titleResId, int layoutResId) {
		mTitleResId = titleResId;
		mLayoutResId = layoutResId;
	}

	public int getTitleResId() {
		return mTitleResId;
	}

	public int getLayoutResId() {
		return mLayoutResId;
	}
}
