package ist.hussein.elcami.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ist.hussein.elcami.R;
import ist.hussein.elcami.components.ECCircleView;
import ist.hussein.elcami.models.ECZikhir;
import ist.hussein.elcami.protocols.CallersDelegate;

public class ECPageAdapter extends PagerAdapter {
	private Context mContext;

	public ECPageAdapter(Context context) {
		mContext = context;
	}

	@Override
	public Object instantiateItem(ViewGroup collection, int position) {
		ECPagerEnum ecPagerEnum = ECPagerEnum.values()[position];
		LayoutInflater inflater = LayoutInflater.from(mContext);
		ViewGroup view = (ViewGroup) inflater.inflate(ecPagerEnum.getLayoutResId(), collection, false);
		collection.addView(view);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup collection, int position, Object view) {
		collection.removeView((View) view);
	}

	@Override
	public int getCount() {
		return ECPagerEnum.values().length;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		ECPagerEnum ecPagerEnum = ECPagerEnum.values()[position];
		return mContext.getString(ecPagerEnum.getTitleResId());
	}
}
