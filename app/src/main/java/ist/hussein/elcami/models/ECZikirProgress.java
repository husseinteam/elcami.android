package ist.hussein.elcami.models;

public final class ECZikirProgress {

	private int primaryProgress = 1;
	private int secondaryIndex = 0;

	public int getPrimaryProgress() {
		return primaryProgress;
	}

	public void setPrimaryProgress(int primaryProgress) {
		this.primaryProgress = primaryProgress;
	}

	public int getSecondaryIndex() {
		return secondaryIndex;
	}

	public void incrementPrimaryProgress() {
		this.primaryProgress++;
	}

	public void incrementSecondaryIndex() {
		this.secondaryIndex++;
	}

	public void reset() {
		this.primaryProgress = 1;
		this.secondaryIndex = 0;
	}

}
