package ist.hussein.elcami.models;

import ist.hussein.elcami.protocols.ModelGeneratorInterface;

public final class ECZikhir {

    private String title;
    private String transcript;
    private Integer count;

    public ECZikhir(String title, String transcript, Integer count) {
        this.title = title;
        this.transcript = transcript;
        this.count = count;
    }
    public ECZikhir(String title, String transcript) {
        this(title, transcript, 1);
    }
    public ECZikhir(String title, Integer count) {
        this(title, title, count);
    }
    public ECZikhir(String title) {
        this(title, title, 1);
    }

    public String getTitle() {
        return title;
    }

    public String getTranscript() {
        return transcript;
    }

    public Integer getCount() {
        return count;
    }

}

