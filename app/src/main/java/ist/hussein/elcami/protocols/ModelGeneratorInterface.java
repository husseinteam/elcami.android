package ist.hussein.elcami.protocols;

public interface ModelGeneratorInterface<TModel> {

    TModel[] generate();

}
